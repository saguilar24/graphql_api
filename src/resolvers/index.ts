import queryResolvers from './query';
import typesResolvers from './types';
import mutationResolvers from "./mutation";

export const resolverIndex = {
    ...queryResolvers,
    ...typesResolvers,
    ...mutationResolvers
}

export default resolverIndex;
