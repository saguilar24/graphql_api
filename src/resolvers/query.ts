import {IResolvers} from "@graphql-tools/utils"
import data from "../data";
import { IBook } from "../interfaces/book-interface";
import { IPeople } from "../interfaces/people-interface";

const queryResolvers: IResolvers = {
    Query: {
        hello: (): string => "Hola a la API de graphQL",
        helloWithName: (_: void, args: { name: string }, context: any, info: object) => {
            console.log(info);
            return `Hola ${args.name}`;
        },
        peopleNumber: () => 1894563,
        booksList: (): {status: boolean, message: string, list: Array<IBook>} => {
            return {
                status: true,
                message: "lista de libros correctamente cargada",
                list: data.books
            };
        },
        peopleList: (): {status: boolean, message: string, list: Array<IPeople>} => {
            return {
                status: true,
                message: "lista de personas correctamente cargada",
                list: data.people
            };
        },
        book: (_: void, args:{ id: string }): {status: boolean, message: string, item: IBook} => {
            const bookFind = data.books.filter(
                (value) => value.id  === args.id
            )[0];
            return {
                status: bookFind === undefined ? false : true,
                message: bookFind === undefined ? `Libro con el id ${args.id} no ha sido encontrado` : `Libro con el id ${args.id} ha sido encontrado`,
                item: bookFind
            };
        },
        people: (_: void, args:{ id: string }): {status: boolean, message: string, item: IPeople} => {
            const poepleFind = data.people.filter(
                (value) => value.id  === args.id
            )[0];
            return {
                status: poepleFind === undefined ? false : true,
                message: poepleFind === undefined ? `Persona con el id ${args.id} no ha sido encontrada` : `Persona con el id ${args.id} ha sido encontrada`,
                item: poepleFind
            };
        }
    },
};

export default queryResolvers
