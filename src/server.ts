import { ApolloServer, gql } from "apollo-server-express";
import compression from "compression";
import express, { Application } from "express";
import { GraphQLSchema } from "graphql";
import { createServer, Server } from "http";

class GraphQLServer {
    // Propiedades
    private app!: Application;
    private httpServer!: Server;
    private readonly DEFAULT_PORT = 3025;
    private schema!: GraphQLSchema;

    constructor(schema: GraphQLSchema ) {
        if(schema === undefined) {
            throw new Error("Necesitamos un esquema de GraphQL para trabajar con APIs GraphQL");
        }
        this.schema = schema;
        this.init();
    }

    init() {
        this.configExpress();
        this.configApolloServerExpress();
        this.configRoutes();
    }

    private configExpress() {
        this.app = express();
        this.app.use(compression());
        this.httpServer = createServer(this.app);

    }

    private async configApolloServerExpress() {
        const apolloServer = new ApolloServer({
            schema: this.schema,
            introspection: true
        });

        await apolloServer.start();

        apolloServer.applyMiddleware({ app: this.app, cors: true })
    }

    private configRoutes() { 
        this.app.get("/hello", (_, res) => {
            res.send("Bienvenid@s al primer proyecto");
        });
    
        this.app.get("/", (_, res) => {
            res.redirect("/graphql");
        });
    }

    listen(callback: (port: number) => void): void {
        this.httpServer.listen(+this.DEFAULT_PORT, () => {
            callback(+this.DEFAULT_PORT)
        })
    }
}

export default GraphQLServer;

function makeExecutableSchema(arg0: { typeDefs: import("graphql").DocumentNode; resolvers: { Query: { hello: () => string; helloWithName: (_: void, args: { name: string; }, context: any, info: object) => string; peopleNumber: () => number; }; }; }): GraphQLSchema {
    throw new Error("Function not implemented.");
}
